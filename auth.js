const jwt = require('jsonwebtoken');

const config = require('./config');
const model = require('./model');

const { ErrorHandler } = require('./helpers/response');

const createNewToken = (userId) => {
  const token = jwt.sign({ userId }, config.jwtsecret);
  return model.createToken(token, userId, config.tokenDurationMinutes).then(() => token);
};

const invalidateToken = (token) => {
  return model.invalidateToken(token);
};

const verifyToken = (req, res, next) => {
  const token = req.token;
  if (!token) {
    next(new ErrorHandler(403, 'No token provided'));
  }

  try {
    var decoded = jwt.verify(token, config.jwtsecret, { ignoreExpiration : true });
    req.userId = decoded.userId;

    model.validateAndExtendToken(token, config.tokenDurationMinutes).then(isTokenValid => {
      console.log('istokenvalid', isTokenValid);
      if (!isTokenValid) {
        next(new ErrorHandler(403, 'Invalid token'));
      } else {
        next();
      }
    });
  } catch(err) {
    next(new ErrorHandler(403, 'Invalid token'));
  }
};

module.exports = {
  createNewToken,
  invalidateToken,
  verifyToken
};
