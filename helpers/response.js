const mung = require('express-mung');

class ErrorHandler extends Error {
  constructor(statusCode, message) {
    super();
    this.statusCode = statusCode;
    this.message = message;
  }
}

const handleError = (err, res) => {
  const { statusCode, message } = err;
  res.status(statusCode).json({
    status: "error",
    statusCode,
    message
  });
};

const handleSuccess = (body, req, res) => {
  body.status = 'success';
  return body;
};

module.exports = {
  ErrorHandler,
  handleError,
  handleSuccess: mung.json(handleSuccess)
};