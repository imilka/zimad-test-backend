const db = require('./db');

const createUser = (id, password) => {
  return db.then(connection => {
    return connection.query(`INSERT INTO Users (id, password) 
                             VALUES ('${id}', '${password}')`);
  });
};

const fetchUser = (id) => {
  return db.then(connection => {
    return connection.query(`SELECT * FROM Users 
                             WHERE id = '${id}' 
                             LIMIT 1`)
    .then(users => {
      return users.length > 0 ? users[0] : null;
    });
  });
};

const createToken = (token, userId, validDurationMinutes) => {
  return db.then(connection => {
    return connection.query(`INSERT INTO Tokens (token, userId, expiresAt) 
                             VALUES ('${token}', '${userId}', NOW() + INTERVAL ${validDurationMinutes} MINUTE)`);
  });
};

const validateAndExtendToken = (token, validDurationMinutes) => {
  return db.then(connection => {
    return deleteExpiredTokens(connection).then(() => {
      return extendToken(connection, token, validDurationMinutes);
    }).then(tokenExtended => {
      return tokenExtended;
    });
  });
};

const invalidateToken = (token) => {
  return db.then(connection => {
    return connection.query(`DELETE FROM Tokens 
                             WHERE token = '${token}'`);
  });
};

const deleteExpiredTokens = (connection) => {
  return connection.query(`DELETE FROM Tokens 
                           WHERE expiresAt < NOW()`);
};

const extendToken = (connection, token, validDurationMinutes) => {
  return connection.query(`UPDATE Tokens 
                           SET expiresAt = NOW() + INTERVAL ${validDurationMinutes} MINUTE 
                           WHERE token = '${token}'`)
  .then(tokenCountResult => {
    return tokenCountResult.affectedRows > 0;  
  });
};

module.exports = {
  createUser,
  fetchUser,
  createToken,
  validateAndExtendToken,
  invalidateToken
};