const bcrypt = require('bcryptjs');

const model = require('../model');
const auth = require('../auth');

const { ErrorHandler } = require('../helpers/response');

const regexPhoneNumber = /^\+\d{10,}$/;
const regexEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
const minimalPasswordLength = 8;

const signUp = (req, res, next) => {
  const id = req.body.id;
  const password = req.body.password;

  if (!regexPhoneNumber.test(id) && !regexEmail.test(id)) {
    next(new ErrorHandler(400, 'Invalid id format'));
  }

  if (password.length < minimalPasswordLength) {
    next(new ErrorHandler(400, 'Password is too short'));
  }

  const hashedPassword = bcrypt.hashSync(password);
  model.createUser(id, hashedPassword).then(() => {
    return auth.createNewToken(id);
  }).then(token => {
    res.send({token});
  }).catch(err => {
    next(new ErrorHandler(400, err.code));
  });
};

const signIn = (req, res, next) => {
  const id = req.body.id;
  const password = req.body.password;

  if (!id || !password) {
    next(new ErrorHandler(400, 'Empty credentials'));
  }

  model.fetchUser(id).then(user => {
    if (!user) {
      next(new ErrorHandler(400, 'Invalid credentials'));
    }

    const passwordCheckResult = bcrypt.compareSync(password, user.password);
    if (!passwordCheckResult) {
      next(new ErrorHandler(400, 'Invalid credentials'));
    }

    return auth.createNewToken(id);
  }).then(token => { 
    res.send({token});
  }).catch(err => {
    next(new ErrorHandler(400, err.code));
  })
};

const logOut = (req, res) => {
  return auth.invalidateToken(req.token).then(() => {
    res.send({});
  });
};

module.exports = {
  signUp,
  signIn,
  logOut
};