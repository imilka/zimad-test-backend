const tcpp = require('tcp-ping');

const info = (req, res) => {
  res.send({
    id: req.userId
  });
};

const latency = (req, res) => {
  tcpp.ping({address: 'google.com'}, (err, data) => {
    res.send(data);
  });
};

module.exports = {
  info,
  latency
};