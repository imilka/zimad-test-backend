const port = process.env.PORT;

const dbhost = process.env.DB_HOST;
const dbport = process.env.DB_PORT;
const dbuser = process.env.DB_USER;
const dbpassword = process.env.DB_PASS;
const dbname = process.env.DB_NAME;

const jwtsecret = process.env.JWT_SECRET;
const tokenDurationMinutes = process.env.TOKEN_DURATION_MINUTES;

module.exports = {
  port,
  dbhost,
  dbport,
  dbuser,
  dbpassword,
  dbname,
  jwtsecret,
  tokenDurationMinutes
};