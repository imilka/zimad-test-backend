const mysql = require('promise-mysql');

const config = require('./config');

const pool = mysql.createPool({
  host: config.dbhost,
  port: config.dbport,
  user: config.dbuser,
  password: config.dbpassword,
  database: config.dbname,
  connectionLimit: 10,
});

module.exports = pool.then(p => {
  return p.getConnection();
});