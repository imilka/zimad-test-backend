require('dotenv').config();
const express = require('express');
const bearerToken = require('express-bearer-token');

const config = require('./config');
const auth = require('./auth');
const authController = require('./controllers/auth');
const mainController = require('./controllers/main');

const { handleError, handleSuccess } = require('./helpers/response');

const app = express();
app.use(express.urlencoded());
app.use(bearerToken());
app.use(handleSuccess);

app.post('/signup', authController.signUp);
app.post('/signin', authController.signIn);
app.get('/logout', auth.verifyToken, authController.logOut);

app.get('/info', auth.verifyToken, mainController.info);
app.get('/latency', auth.verifyToken, mainController.latency);

app.use((err, req, res, next) => {
  handleError(err, res);
});

app.listen(config.port);